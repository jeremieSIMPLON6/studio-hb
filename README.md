

* Environnement

> Ubuntu 18.04
> VSCode 1.23.0
> Google Chrome 72.0.3626.121

* Technologies

> Bootstrap 4.3.1
> Scrollreveal 4.0.0
> Sass 1.4.9



* Ma touche personnelle

  J'ai veillé à ce qu'elle ne dénature/modifie pas la maquette initial

> Animation au scroll
> Animation au hover
> Ajout d'un background sur les textes centraux
> Ajout d'un boutton switch qui charge avec le JS une feuille CSS alternative

  Ce deuxième style se veut la version "dark" de la première:
  > Moderne
  > Meilleures contrastes
  > Moins agréssif pour l'oeil


* Conclusion

> La maquette est assez fidèlement représentée, petit émol sur le logo qui je n'arrive pas à placer plus précisément.
> La page est normalement totalement responsive, je vous invite à tester.

> Attention, je n'ai pas pu vérifier le bon fonctionnement de la page sur Safari.