

// Switch CSS file


var swicth = true;

document.getElementById("switch").addEventListener("click", function () {
  if (swicth) {
    document.querySelector("link[href='css/style.css']").href = "css/style-alt.css";
    swicth = false;
  } else {
    document.querySelector("link[href='css/style-alt.css']").href = "css/style.css";
    swicth = true;
  }
});

// Scroll JS

ScrollReveal().reveal('.navbar', {
  delay: 200,
  duration: 300,
  reset: true,
  origin: "top",
  distance: "20px"
});
ScrollReveal().reveal('.text-center', {
  delay: 200,
  duration: 300,
  reset: true,
  origin: "top",
  distance: "20px"
});
ScrollReveal().reveal('.main-left', {
  delay: 200,
  duration: 300,
  reset: true,
  origin: "left",
  distance: "20px"
});
ScrollReveal().reveal('.main-right', {
  delay: 200,
  duration: 300,
  reset: true,
  origin: "right",
  distance: "20px"
});
ScrollReveal().reveal('.details-btn', {
  delay: 200,
  duration: 300,
  reset: true,
  origin: "right",
  distance: "20px"
});